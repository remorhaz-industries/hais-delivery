import os

# Put all the music in audio\bgm (of any format) in a list. We'll remove from
# here as we find it. What's left is unused.
unused_bgm = []
for bgm_filename in os.listdir("audio\\bgm"):
    # Strip off the file extension, because we may have the same music in .m4a,
    # .ogg, etc and RPG Maker MV doesn't reference the extension anyway.
    bgm_prefix = bgm_filename.split('.')[0]
    if not bgm_prefix in unused_bgm:
        #print("Found bgm " + bgm_prefix + ".")
        unused_bgm.append(bgm_prefix)

data_files = os.listdir("data")
# HACK: Sound effects may have the same names as music and it's hard to tell the
# difference, so I assume there's no music in Animations.json.
data_files.remove("Animations.json")
for data_filename in data_files:
    used_bgm = []
    with open("data\\" + data_filename) as data_file:
        contents = data_file.read()
        # Any music that matches our unused_bgm list will be removed after we're
        # done iterating through the list.
        for bgm in unused_bgm:
            if bgm in contents:
                used_bgm.append(bgm)
                print("Found " + bgm + " in " + data_filename + ".");
    for bgm in used_bgm:
        unused_bgm.remove(bgm)

if len(unused_bgm) == 0:
    print("No unused music found.")
else:
    for bgm in unused_bgm:
        print("Music", bgm, "is unused.")
