Hai's Delivery

The first (and only) in (not) a series of RPGs by Ward Childress and Remorhaz Industries.

In an attempt to learn RPG Maker MV, and to spur my cousin on to designing an RPG of his own, I decided to make a little something myself.

This is a modest attempt at a classic JRPG with a silly heroine going on a silly quest. Play as Hai as she is tasked with delivering a mysterious package to a mysterious wizard in a mysterious tower. What could possibly go wrong?

Oh! You can play this game online at: https://remorhaz-industries.gitlab.io/hais-delivery/
